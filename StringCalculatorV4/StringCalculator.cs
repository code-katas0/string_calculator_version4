﻿using System;
using System.Collections.Generic;

namespace StringCalculatorV4
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            
            var delimiterList = GetValidDelimiters(numbers);
            var numberArray = SplitByValidDelimiters(numbers,delimiterList);

            CheckInvalidDelimiters(numberArray);

            var convertedNumbList =GetConvertedNumbers(numberArray);

            CheckNegativeNumbers(convertedNumbList);

            convertedNumbList = IngoreNumberGreaterThanThousand(convertedNumbList);
           
            var total = GetTotal(convertedNumbList);

            return total;
        }

        private List<string> GetValidDelimiters(string numbers)
        {
            var delimiterList = new List<string>(new string[] { ",", "\n" });
           
            if (numbers.Contains("//["))
            {
                delimiterList.Clear();
               
                var delimiters = numbers.Substring(numbers.IndexOf("["), numbers.LastIndexOf("]")-1);
                
                foreach (var delimiter in delimiters.Split('[', ']'))
                {
                    delimiterList.Add(delimiter);
                }
            
            } 
            return delimiterList;
        }

        private string[] SplitByValidDelimiters(string numbers, List<string> delimiterList)
        {
            if (numbers.Contains("//"))
            {
                numbers = string.Concat(numbers.Split('/'));
                var delimiters = numbers.Substring(0, numbers.IndexOf("\n"));
                delimiterList.Add(delimiters);
                numbers = string.Concat(numbers.Split('/', '\n'));
            }
            if (numbers.Contains("//["))
            {
                numbers = string.Concat(numbers.Split('/', '\n', '[', ']'));
            }
            return numbers.Split(delimiterList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        private void CheckInvalidDelimiters(string[] numbArray)
        {
            var numberCharArray = string.Concat(numbArray).ToCharArray();
            var invalidDelimiters = string.Empty;
            
            foreach (var number in numberCharArray)
            {
                if (!int.TryParse(number.ToString(), out int validNumber) && !number.ToString().Contains("-"))
                {
                    invalidDelimiters = string.Join(" ", invalidDelimiters, number);
                }
            }

            if (!string.IsNullOrEmpty(invalidDelimiters))
            {
                throw new Exception(string.Concat("Invalid Delimiter !." ,invalidDelimiters));
            }
        }

        private List<int> GetConvertedNumbers(string[] numbArray)
        {
            var numberList = new List<int>();

            foreach (var number in numbArray)
            {
                numberList.Add(int.Parse(number));
            }
            return numberList;
        }

        private void CheckNegativeNumbers(List<int> numberlist)
        {
            var negativeNumbers = string.Empty;

            foreach (var number in numberlist)
            {
                if (number < 0)
                {
                    negativeNumbers = string.Join(" ", negativeNumbers, number);
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }
        }
        
        private List<int> IngoreNumberGreaterThanThousand(List<int> convertedNumbersList)
        {
            var numberList = new List<int>();

            foreach (var number in convertedNumbersList)
            {
                if (number <= 1000)
                {
                    numberList.Add(number);
                }
            }

            return numberList;
        }

        private int GetTotal(List<int> numbersList)
        {
            var sum = 0;
            numbersList.ForEach(l => sum += l);
            return sum;
        }
    }
}
