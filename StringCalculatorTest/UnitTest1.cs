using NUnit.Framework;
using System;
using StringCalculatorV4;

namespace StringCalculatorTest
{
    public class Tests
    {
        [TestFixture]
        public class StringCalculatorTest
        {
            private StringCalculator _calculator;
            [SetUp]
            public void SetUp()
            {
                _calculator = new StringCalculator();
            }

            [Test]
            public void Given_Empty_String_When_Adding_Then_Return_Zero()
            {
                //Arrange
                var expected = 0;

                //Act
                var actual = _calculator.Add("");

                //Assert
                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Given_One_Number_When_Adding_Then_GivenNumber()
            {
                //Arrange
                var expected = 1;

                //Act
                var actaul = _calculator.Add("1");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Two_Numbers_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 3;

                //Act
                var actaul = _calculator.Add("1,2");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Unlimited_Numbers_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 20;

                //Act
                var actaul = _calculator.Add("10,1,1,1,2,5");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_New_Lines_Between_Numbers_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 6;

                //Act
                var actaul = _calculator.Add("1\n2,3");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Custom_Delimiters_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 3;

                //Act
                var actaul = _calculator.Add("//;\n1;2");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Negative_Numbers_When_Adding_Then_Throw_Expcetion()
            {
                //Arrange
                var expected = "Negatives not Allowed. -3 -4";

                //Act
                var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n2;-3;-4"));

                //Assert
                Assert.AreEqual(expected, exception.Message);
            }

            [Test]
            public void Given_Numbers_When_Adding_Then_Ignore_Numbers_Greater_Thousand()
            {
                //Arrange
                var expected = 3;

                //Act
                var actaul = _calculator.Add("//;\n2;1001;1");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Any_Length_Of_Delemiliters_When_Adding()
            {
                //Arrange
                var expected = 6;

                //Act
                var actaul = _calculator.Add("//***\n1***2***3");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Multiple_Delimiters_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 6;

                //Act
                var actaul = _calculator.Add("//[*][%]\n1*2%3");

                //Assert
                Assert.AreEqual(expected, actaul);
            }

            [Test]
            public void Given_Invalid_Delimiter_When_Adding_Then_Throw_Exception()
            {
                //Arrange
                var expected = "Invalid Delimiter !. $";

                //Act
                var exception = Assert.Throws<Exception>(() => _calculator.Add("//[*][%]\n1*2$3"));

                //Assert
                Assert.AreEqual(expected, exception.Message);
            }

            [Test]
            public void Given_Unlimited_Multiple_Delimiters_When_Adding_Then_Return_Sum()
            {
                //Arrange
                var expected = 6;

                //Act
                var actaul = _calculator.Add("//[$$*][##%&]\n1$$*2##%&3");

                //Assert
                Assert.AreEqual(expected, actaul);
            }
        }
    }
}